const jwt = require('jsonwebtoken');
const tokenModel = require('../models/token-model');


class TokenService{
    generateTokens(payload){
        const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_KEY, {expiresIn: '30m'});
        const refreshToken = jwt.sign(payload, process.env.JWT_ACCESS_KEY, {expiresIn: '1d'});
        return{accessToken, refreshToken}
    };
    validateAccessToken(token){
        try{
            return jwt.verify(token, process.env.JWT_ACCESS_KEY);
        }catch (e) {
            return null;
        }
    };
    validateRefreshToken(token){
        try{
            return jwt.verify(token, process.env.JWT_REFRESH_KEY);
        }catch (e) {
            return null;
        }
    };
    async saveToken(userid, refreshToken){
        const tokenData = await tokenModel.findOne({user_id:userid});
        if(tokenData){
            tokenData.refreshToken = refreshToken;
            return tokenData.save();
        }
        return await tokenModel.create({user_id:userid, refreshToken});
    };
    async removeToken(refreshToken){
        return await tokenModel.deleteOne({refreshToken});
    };
    async findToken(refreshToken){
        return await tokenModel.findOne({refreshToken});
    };
}

module.exports = new TokenService();