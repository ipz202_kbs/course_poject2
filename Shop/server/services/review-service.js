const UserModel = require("../models/user-model");
const ProductModel = require("../models/product-model");
const ReviewModel = require("../models/review-model")
const ApiError = require('../exceptions/api-errors');

module.exports = new class ReviewService {
    async createReview(user_id, product_id, grade, review){
        const userData = await UserModel.findById(user_id);
        if (!userData){
            throw ApiError.BadRequest('Пользователя с таким id не существует!');
        }
        const productData = await ProductModel.findById(product_id);
        if(!productData){
            throw ApiError.BadRequest('Товар с указаным id не найден!');
        }

        const candidate = await ReviewModel.findOne({product_id:product_id, user_nickname:userData.nickname});
        if(candidate){
            throw ApiError.BadRequest('Вы уже оставили отзыв!');
        }

        const reviewData = await ReviewModel.create({product_id, user_nickname:userData.nickname, grade, review})
        productData.reviews.push(reviewData.id);
        await productData.populate('reviews');

        let total_rating = 0;
        if (productData.reviews !== 0){
            productData.reviews.forEach(item => {
                total_rating += item.grade
            })
            productData.total_rating = (total_rating/productData.reviews.length).toFixed(1);
        }
        await productData.save()
        return productData.reviews;
    }
    // async getReviews(product_id) {
    //     const productData = await ProductModel.findById(product_id);
    //     if(!productData){
    //         throw ApiError.BadRequest('Товар с указаным id не найден!');
    //     }
    //     const reviewData = await ProductModel.findById(product_id).populate('reviews');
    //     return reviewData.reviews;
    // }
}