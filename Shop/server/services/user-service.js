const UserModel = require("../models/user-model");
const CartModel = require("../models/cart-model");
const bcrypt = require("bcrypt");
const uuid = require('uuid');
const mailService = require('./mail-service');
const tokenService = require('./Token-service');
const cartService =require('./cart-service')
const UserDto = require('../dtos/user-dto');
const ApiError = require('../exceptions/api-errors')


class UserService{
    async registration(name, age, nickname, email, password){
        const candidate = await UserModel.findOne({email});
        if(candidate){
            throw ApiError.BadRequest(`Данный email занят, попробуйте другой`);
        }

        const hashPassword = await bcrypt.hash(password, 5);
        const activationLink = uuid.v4();
        const cart = await cartService.create();
        const user = await UserModel.create({name,  age, nickname, email, password:hashPassword, activationLink, cart_id: cart.id});
        await mailService.sendActivationMail(email, `${process.env.API_URL}/api/user/activate/${activationLink}`);

        const userDto = new UserDto(user);
        const tokens = tokenService.generateTokens({...userDto});
        await tokenService.saveToken(userDto.id, tokens.refreshToken);
        return{...tokens, user:userDto};
    }
    async activation(activationLink){
        const user = await UserModel.findOne({activationLink});
        if(!user){
            throw ApiError.BadRequest("Ссылка активации не валидная!!!");
        }
        user.isActivated = true;
        await user.save();
    }
    async login(email, password){
        const user = await UserModel.findOne({email});
        if(!user){
            throw ApiError.BadRequest("Пльзователь с таким email не существует.");
        }
        const isPasswordCorrect = await bcrypt.compare(password, user.password);
        if(!isPasswordCorrect){
            throw ApiError.BadRequest("Не верный пароль");
        }
        const userDto = new UserDto(user);

        const tokens = tokenService.generateTokens({...userDto});

        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return{...tokens, user:userDto};
    }
    async logout(refreshToken){
        return await tokenService.removeToken(refreshToken);
    }
    async refresh(refreshToken){
        if(!refreshToken){
            throw ApiError.UnauthorizedError();
        }
        const userData = tokenService.validateRefreshToken(refreshToken);
        const tokenFromDB = await tokenService.findToken(refreshToken);
        if(!userData || !tokenFromDB){
            throw ApiError.UnauthorizedError();
        }
        const user = await  UserModel.findById(userData.id)
        const userDto = new UserDto(user);

        const tokens = tokenService.generateTokens({...userDto});

        await tokenService.saveToken(userDto.id, tokens.refreshToken);
        return{...tokens, user:userDto};
    }
    async getCart(user_id){
        const user = await UserModel.findById(user_id);
        const cart = await CartModel.findById(user.cart_id);
        return {user, cart};
    }
}

module.exports = new UserService();