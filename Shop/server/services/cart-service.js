const UserModel = require("../models/user-model");
const CartModel = require("../models/cart-model");
const ProductModel = require("../models/product-model");
const ApiError = require('../exceptions/api-errors');

module.exports = new class CartService{
    async create(){
        return await CartModel.create({});
    }
    async addToCart(user_id, product_id){
        const productData = await ProductModel.findById(product_id);
        if(!productData){
            throw ApiError.BadRequest('Товар с указаным id не найден!');
        }
        const userData = await UserModel.findById(user_id);
        if (!userData){
            throw ApiError.BadRequest('Пользователя с таким id не существует!');
        }
        const cart = await CartModel.findById(userData.cart_id);
        if (!cart){
            throw ApiError.BadRequest('У пользователя нет корзины!');
        }
        let indexFound = cart.items.findIndex(item => (item.productId == product_id));
        if(indexFound === -1){
            cart.items.push({
                productId: product_id,
                quantity: 1,
                price: productData.price,
                total: parseInt(productData.price)
            })
        }
        if(indexFound !== -1){
            cart.items[indexFound].quantity += 1;
            let quantity = cart.items[indexFound].quantity;
            cart.items[indexFound].total = (productData.price *  quantity);
         }
        cart.subTotal = 0;
        cart.items.forEach(item => {
            cart.subTotal += item.total;
        })

        return await cart.save();
    }
    async deleteFromCart(user_id, product_id){
        const userData = await UserModel.findById(user_id);
        if (!userData){
            throw ApiError.BadRequest('Пользователя с таким id не существует!');
        }
        const cart = await CartModel.findById(userData.cart_id);
        if (!cart){
            throw ApiError.BadRequest('У пользователя нет корзины!');
        }
        let indexFound = cart.items.findIndex(item => (item.productId == product_id));

        const productData = await ProductModel.findById(product_id);
        if(!productData){
            cart.items.splice(indexFound, 1);
            cart.subTotal = 0;
            cart.items.forEach(item => {
                cart.subTotal += item.total;
            })
            return await cart.save();
        }

        if(cart.items[indexFound].quantity !== 0){
            cart.items[indexFound].quantity -= 1;
            let quantity = cart.items[indexFound].quantity;
            if(quantity === 0){
                cart.items.splice(indexFound, 1);
                cart.subTotal = 0;
                cart.items.forEach(item => {
                    cart.subTotal += item.total;
                })
                return await cart.save();
            }
            cart.items[indexFound].total = (cart.items[indexFound].price *  quantity);
        }
        cart.subTotal = 0;
        cart.items.forEach(item => {
            cart.subTotal += item.total;
        })
        return await cart.save();
    }

    async getAllProducts(user_id){
        const userData = await UserModel.findById(user_id);
        if (!userData){
            throw ApiError.BadRequest('Пользователя с таким id не существует!');
        }
        const cartData = await CartModel.findById(userData.cart_id);
        if (!cartData){
            throw ApiError.BadRequest('У пользователя нет корзины!');
        }
        return {cartData};
    }
}
