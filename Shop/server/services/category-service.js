const CategoryModel = require("../models/category-model");
const ProductModel = require("../models/product-model");
const ApiError = require('../exceptions/api-errors');

class CategoryService{
    async createCategory(category){
        const candidate = await CategoryModel.findOne({category_name:category});
        if (candidate){
            throw ApiError.BadRequest('Такая категория уже существует.');
        }
        return await CategoryModel.create({category_name:category});
    }
    async deleteCategory(category){
        const candidate = await CategoryModel.findOne({category_name:category});
        if (!candidate){
            throw ApiError.BadRequest('Такой категории не существует.');
        }
        await ProductModel.deleteMany({category_id:candidate._id})
        return await CategoryModel.deleteOne({category_name:category});
    }
    async getAllCategory(){
        const categories = await CategoryModel.find();
        if(!categories){
            return null;
        }
        return categories;
    }
}
module.exports = new CategoryService();