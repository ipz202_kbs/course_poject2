const ProductModel = require("../models/product-model");
const CategoryModel = require("../models/category-model");
const ReviewModel = require("../models/review-model")

const CategoryService = require("./category-service");

const ApiError = require('../exceptions/api-errors')
const fs = require("fs");

class ProductService{
    async createProduct(category, photo, price, name, fortress, volume, excerpt, country_of_origin, description){
        if(!category){
            throw ApiError.BadRequest('Выберите категорию!!!');
        }
        const candidate = await ProductModel.findOne({name:name});
        if (candidate){
            throw ApiError.BadRequest('Такай товар уже существует.');
        }
        const candidateData = await CategoryModel.findOne({category_name:category});
        if(!candidateData){
            const newCategory = await CategoryService.createCategory(category);
            return await ProductModel.create({ photo, price, name, fortress, volume, excerpt, country_of_origin, description, category_id:newCategory.id});
        }
        return await ProductModel.create({ photo, price, name, fortress, volume, excerpt, country_of_origin, description, category_id:candidateData.id});
    }

    async getBySearch(name){
        if(!name){
            return await ProductModel.find();
        }
        return await ProductModel.find({name:{$regex: name}});
    }

    async getProducts(params){

        if(params.categoryId === 'false')
            return await ProductModel.find();

        if(params.minPrice === '')
            params.minPrice = 0
        if(params.maxPrice === '')
            params.maxPrice = 9000000;
        if(params.minVolume === '')
            params.minVolume = 0;
        if(params.maxVolume === '')
            params.maxVolume = 100;
        if(params.minFortress === '')
            params.minFortress = 0;
        if(params.maxFortress === '')
            params.maxFortress = 100;

        if(params.categoryId === '{}' || !params.categoryId){
            const data = await ProductModel.find({
                $and:[
                    {
                        $and:[
                            {price: {$gte: params.minPrice}},
                            {price: {$lt: params.maxPrice}}
                        ]
                    },
                    {
                        $and:[
                            {volume: {$gte: params.minVolume}},
                            {volume: {$lt: params.maxVolume}}
                        ]
                    },
                    {
                        $and:[
                            {fortress: {$gte: params.minFortress}},
                            {fortress: {$lt: params.maxFortress}}
                        ]
                    }
                ]
            });
            return data;
        }else {
            const data = await ProductModel.find({
                $and:[
                    {
                        category_id: params.categoryId
                    },
                    {
                        $and:[
                            {price: {$gte: params.minPrice}},
                            {price: {$lt: params.maxPrice}}
                        ]
                    },
                    {
                        $and:[
                            {volume: {$gte: params.minVolume}},
                            {volume: {$lt: params.maxVolume}}
                        ]
                    },
                    {
                        $and:[
                            {fortress: {$gte: params.minFortress}},
                            {fortress: {$lt: params.maxFortress}}
                        ]
                    }
                ]
            });
            return data;
        }
    }
    async changeOne(id, photo, price, name, fortress, volume, excerpt, country_of_origin, description){
        const filter = { _id: id };
        let update;
        if(photo){
            update = {photo:photo, price:price, name:name, fortress:fortress, volume:volume, excerpt:excerpt, country_of_origin:country_of_origin, description:description}
            const candidate = await ProductModel.findById(id);
            fs.unlink(`./static/${candidate.photo}`, (e) => {
                if(e) console.log(e)});
        }
        else {
            update = {price:price, name:name, fortress:fortress, volume:volume, excerpt:excerpt, country_of_origin:country_of_origin, description:description}
        }
        return await ProductModel.findOneAndUpdate(filter, update);
    }


    async deleteOne(product_id){
        if(!product_id){
            throw ApiError.BadRequest('ID товара не указан!');
        }
        const candidate = await ProductModel.findById(product_id);
        if(!candidate){
            throw ApiError.BadRequest('Такого товара не существует!');
        }
        await ReviewModel.deleteMany({product_id: product_id})
        fs.unlink(`./static/${candidate.photo}`, (e) => {
            if(e) console.log(e)});

       return await ProductModel.findByIdAndDelete(product_id);
    }
    async getOne(product_id){
        if(!product_id){
            throw ApiError.BadRequest('ID товара не указан!');
        }
        const candidate = await ProductModel.findById(product_id).populate('reviews');
        if(!candidate){
            return null;
        }
        candidate.reviews.reverse()
        return candidate;
    }
}
module.exports = new ProductService();