const Router = require('express').Router;
const cartRouter = new Router();
const cartController = require('../controllers/cart-controller');
const authMiddleware = require('../middlewares/auth-middleware');
const isActivated = require('../middlewares/is-activated-middlevare')


cartRouter.post('/',authMiddleware, isActivated,cartController.addToCart);
cartRouter.delete('/',authMiddleware, isActivated,cartController.deleteFromCart);
cartRouter.get('/:id',authMiddleware, isActivated ,cartController.getall);

module.exports = cartRouter;