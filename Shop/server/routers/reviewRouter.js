const Router = require('express').Router;
const reviewRouter = new Router();
const reviewController = require('../controllers/review-controller');
const authMiddleware = require('../middlewares/auth-middleware');
const isActivated = require('../middlewares/is-activated-middlevare')

reviewRouter.post('/',authMiddleware, isActivated, reviewController.createReview);
// reviewRouter.get('/:id', reviewController.getReviews);

module.exports = reviewRouter;