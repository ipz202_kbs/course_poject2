const Router = require('express').Router;
const productRouter = new Router();
const productController = require('../controllers/product-controller');
const authMiddleware = require('../middlewares/auth-middleware');
const checkRoleMiddleware = require('../middlewares/check-role-middleware');

productRouter.post('/',authMiddleware,checkRoleMiddleware,productController.create);
productRouter.delete('/:id',authMiddleware,checkRoleMiddleware,productController.deleteOne);
productRouter.put('/',authMiddleware,checkRoleMiddleware,productController.changeOne);
productRouter.get('/search', productController.getBySearch);
productRouter.get('/', productController.getAll);
productRouter.get('/:id',productController.getOne);


module.exports = productRouter;
