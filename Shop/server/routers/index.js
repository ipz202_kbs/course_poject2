const Router = require('express').Router;
const router = new Router();

const userRouter = require('./userRouter');
const cartRouter = require('./cartRouter');
const categoryRouter = require('./categoryRouter');
const productRouter = require('./productRouter');
const reviewRouter = require('./reviewRouter');

router.use('/user', userRouter);
router.use('/cart', cartRouter);
router.use('/category', categoryRouter);
router.use('/product', productRouter);
router.use('/reviews', reviewRouter);

module.exports = router;
