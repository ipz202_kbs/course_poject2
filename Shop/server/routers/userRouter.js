const Router = require('express').Router;
const userRouter = new Router();
const userController = require('../controllers/user-controller');
const {body} = require('express-validator');
const authMiddleware = require('../middlewares/auth-middleware');
const isActivated = require('../middlewares/is-activated-middlevare')


userRouter.post('/registration',
    body('name').isLength({min:2}),
    body('age'),
    body('email').isEmail(),
    body('password').isLength({min: 8, max: 28}),
    userController.registration);
userRouter.post('/login', userController.login);
userRouter.post('/logout', userController.logout);
userRouter.get('/activate/:link', userController.activate);
userRouter.get('/refresh',userController.refresh);
userRouter.get('/cart',authMiddleware,isActivated,userController.getCart);

module.exports = userRouter;
