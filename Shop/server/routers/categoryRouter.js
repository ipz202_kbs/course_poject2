const Router = require('express').Router;
const categoryRouter = new Router();
const categoryController = require('../controllers/category-controller');
const authMiddleware = require('../middlewares/auth-middleware');
const checkRoleMiddleware = require('../middlewares/check-role-middleware');


categoryRouter.post('/create',authMiddleware, checkRoleMiddleware,categoryController.create);
categoryRouter.delete('/:category',authMiddleware, checkRoleMiddleware,categoryController.delete);
categoryRouter.get('/get', categoryController.getall);

module.exports = categoryRouter;
