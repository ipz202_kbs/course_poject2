const mongoose = require("mongoose")

const reviewSchema = new mongoose.Schema({
    product_id:{type: mongoose.Types.ObjectId, ref: 'Product'},

    user_nickname:{type: String, required: true},

    grade:{type: Number, required : true, default: 0},

    review:{type: String},

});


module.exports = mongoose.model('Review', reviewSchema);