const mongoose = require("mongoose");

const tokenSchema  = new mongoose.Schema({
    user_id:{type: mongoose.Types.ObjectId, ref:'User'},

    refreshToken:{type: String, required:true}
});

module.exports = mongoose.model('Token', tokenSchema);