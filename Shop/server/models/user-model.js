const mongoose = require("mongoose");

const userSchema  = new mongoose.Schema({
    name:{type: String,required: true},
    age:{type:Number,required:true,min: 18},
    nickname:{type: String,required: true},
    email:{type: String, unique: true, required: true},
    password:{type: String, required:true},
    role:{type:String,default: "USER", required:true},
    isActivated:{type: Boolean, default:false},
    activationLink:{type: String},
    cart_id:{type: mongoose.Types.ObjectId, ref:'Cart'},
});

module.exports = mongoose.model('User', userSchema);