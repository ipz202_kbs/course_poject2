const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
    photo:{type:String, required: true},

    price:{type: Number, required: true},

    name:{type: String, required: true},

    fortress:{type: Number, required: true},

    volume:{type: Number, required: true},

    excerpt:{type: Number, default: 0},

    country_of_origin:{type: String},

    description:{type: String, required: true},

    total_rating:{type: Number, default : 0},

    reviews:[{type: mongoose.Types.ObjectId, ref: 'Review'}],

    category_id:{type: mongoose.Types.ObjectId, ref: 'Category'}
});

module.exports = mongoose.model('Product', productSchema);