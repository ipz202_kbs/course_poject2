const reviewService = require('../services/review-service');
const ApiError = require('../exceptions/api-errors');

module.exports = new class reviewController{
    async createReview(req, res, next){
        try{
            const{user_id, product_id, grade, review} = req.body;
            const reviewData = await reviewService.createReview(user_id, product_id, grade, review);
            return res.status(200).json(reviewData);
        }catch (e){
            next(e);
        }
    }
    // async getReviews(req, res, next){
    //     try{
    //         const {id} = req.params;
    //         const reviewsData = await reviewService.getReviews(id);
    //         return res.status(200).json(reviewsData)
    //     }catch (e){
    //         next(e);
    //     }
    // }
}