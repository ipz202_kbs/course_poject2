const userService = require('../services/user-service');
const {validationResult} = require('express-validator');
const ApiError = require('../exceptions/api-errors');

class UserController{
    async registration(req, res, next){
        try{
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                console.log(errors);
                return next(ApiError.BadRequest('Пароль или email не валидный! Попробуйте ещё.', errors.array()));
            }
            const {name, age, nickname, email, password} = req.body;
            const userData = await userService.registration(name, age, nickname, email, password);
            res.cookie('refreshToken', userData.refreshToken, {maxAge:10 * 24 * 60 * 60 * 1000, httpOnly: true});
            return res.status(200).json(userData.accessToken);
        }catch (e){
            next(e);
        }
    }
    async login(req, res, next){
        try{
            const {email, password} = req.body;
            const userData = await userService.login(email, password);
            res.cookie('refreshToken', userData.refreshToken, {maxAge:10 * 24 * 60 * 60 * 1000, httpOnly: true});
            return res.status(200).json(userData.accessToken);
        }catch (e){
            next(e);
        }
    }
    async logout(req, res, next){
        try{
            const {refreshToken} = req.cookies;
            await userService.logout(refreshToken);
            res.clearCookie('refreshToken');
            return res.status(200);
        }catch (e){
            next(e);
        }
    }
    async activate(req, res, next){
        try{
            const activationLink = req.params.link;
            await userService.activation(activationLink);
            return res.status(200).redirect(process.env.CLIENT_URL);
        }catch (e){
            next(e);
        }
    }
    async refresh(req, res, next){
        try{
            const {refreshToken} = req.cookies;
            const userData = await userService.refresh(refreshToken);
            res.cookie('refreshToken', userData.refreshToken, {maxAge:10 * 24 * 60 * 60 * 1000, httpOnly: true});
            return res.status(200).json(userData.accessToken);
        }catch (e){
            next(e);
        }
    }
    async getCart(req, res, next){
        try{
            const {user_id} = req.body;
            const cart = await userService.getCart(user_id);
            return res.status(200).json(cart);
        }catch (e){
            next(e);
        }
    }
}
module.exports = new UserController();