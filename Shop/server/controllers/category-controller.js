const categoryService = require('../services/category-service');
const ApiError = require('../exceptions/api-errors');

class CategoryController{
    async create(req, res, next){
        try{
            const {category} = req.body;
            const categoryData = await categoryService.createCategory(category);
            return res.status(200).json(categoryData);
        }catch (e){
            next(e);
        }
    }
    async delete(req, res, next){
        try{
            const {category} = req.params;
            const categoryData = await categoryService.deleteCategory(category);
            return res.status(200).json(categoryData);
        }catch (e){
            next(e);
        }
    }
    async getall(req, res, next){
        try{
            return res.status(200).json(await categoryService.getAllCategory());
        }catch (e){
            next(e);
        }
    }
}
module.exports = new CategoryController();