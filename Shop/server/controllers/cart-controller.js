const cartService = require('../services/cart-service');
const ApiError = require('../exceptions/api-errors');

module.exports = new class CartController{
    async addToCart(req, res, next){
        try{
            const{user_id, product_id} = req.body;
            const cartData = await cartService.addToCart(user_id, product_id);
            return res.status(200).json(cartData);
        }catch (e){
            next(e);
        }
    }
    async deleteFromCart(req, res, next){
        try{
            const {user_id, product_id} = req.query;
            const cartData = await cartService.deleteFromCart(user_id, product_id);
            return res.status(200).json(cartData);
        }catch (e){
            next(e);
        }
    }
    async getall(req, res, next){
        try{
            const {id} = req.params;
            const cartData = await cartService.getAllProducts(id);
            return res.status(200).json(cartData);
        }catch (e){
            next(e);
        }
    }
}
