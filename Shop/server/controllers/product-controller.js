const productService = require('../services/product-service');
const ApiError = require('../exceptions/api-errors');
const uuid = require('uuid');
const path = require('path');

class ProductController{
    async create(req, res, next){
        try{
            const {category, price, name, fortress, volume, excerpt, country_of_origin, description} = req.body;
            const {img} = req.files;
            const imgName =uuid.v4() + ".jpg";
            img.mv(path.resolve(__dirname, '..', 'static', imgName));
            const productData = await productService.createProduct(category, imgName, price, name, fortress, volume, excerpt, country_of_origin, description);
            return res.status(200).json(productData);
        }catch (e){
            next(e);
        }
    }
    async deleteOne(req, res, next){
        try{
            const {id} = req.params;
            const deletedProduct = await productService.deleteOne(id);
            return res.status(200).json(deletedProduct);
        }catch (e){
            next(e);
        }
    }
    async changeOne(req, res, next){
        try{
            const {id, price, name, fortress, volume, excerpt, country_of_origin, description} = req.body;
            let newImgName;

            if(req.files){
                const {img} = req.files;
                newImgName =uuid.v4() + ".jpg";
                img.mv(path.resolve(__dirname, '..', 'static', newImgName));
            }
            else
                newImgName = null;
            const changedProduct = await productService.changeOne(id, newImgName, price, name, fortress, volume, excerpt, country_of_origin, description);
            return res.status(200).json(changedProduct);
        }catch (e){
            next(e);
        }
    }
    async getBySearch(req, res, next){
        try{
            const {name} = req.query;
            const result = await productService.getBySearch(name);
            return res.status(200).json(result)
        }catch (e){
            next(e);
        }

    }

    async getAll(req, res, next){
        try{
            const params = req.query;
            const result = await productService.getProducts(params);
            return res.status(200).json(result)
        }catch (e){
            next(e);
        }
    }
    async getOne(req, res, next){
        try{
            const {id} = req.params;
            const product = await productService.getOne(id);
            return res.status(200).json(product);
        }catch (e){
            next(e);
        }
    }
}
module.exports = new ProductController();