const ApiError = require('../exceptions/api-errors');
const tokenService = require('../services/Token-service');

module.exports =  (req, res, next) =>{
    try {
        const accessToken = req.headers.authorization.split(' ')[1];
        const userData = tokenService.validateAccessToken(accessToken);
        if(!userData.isActivated){
            return next(ApiError.BadRequest('Аккаунт не активированый'));
        }
        req.user = userData;
        next();
    }catch (e) {
        return next(ApiError.UnauthorizedError());
    }
}