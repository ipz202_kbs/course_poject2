import React, {useContext, useEffect} from 'react';
import {Button, Container, Form} from "react-bootstrap";
import Card from "react-bootstrap/Card";
import {Context} from "../index";
import CartItemsList from "../components/CartItemsList";
import CartService from "../services/CartService";
import {observer} from "mobx-react-lite";
import {useNavigate} from "react-router-dom";

const Cart = () => {
    const {cart} = useContext(Context)
    const {user} = useContext(Context)
    const navigate = useNavigate();
    useEffect(() => {
        CartService.fetchProducts(user.getUser.user.id).then(response => {
            cart.setId(response.data.cartData._id)
            cart.setTotal(response.data.cartData.subTotal)
            cart.setCartItems(response.data.cartData.items)
        })
    },[])
    return (
        <Container className="mt-3 d-flex pl-3 pr-3 d-flex justify-content-between">
            <Card style={{width: 600}} className="p-4 mt-3">
                <h3 >Корзина</h3>
                <CartItemsList/>
            </Card>
            <Card style={{width: 300, height: 300}} className="p-4 mt-3 ms-3 d-flex flex-column align-items-center">
                <strong>Общая стоимость</strong>
                <p>{cart.getTotalPrice.toFixed(2)} ГРН.</p>
                <Button variant="outline-success" className={'px-2'}>Оформить заказ.</Button>
            </Card>
        </Container>
    );
};

export default observer(Cart);