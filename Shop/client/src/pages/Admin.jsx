import React, {useState} from 'react';
import {Container} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import CreateCategory from "../components/modals/CreateCategory";
import CreateProduct from "../components/modals/CreateProduct";
import DeleteCategory from "../components/modals/DeleteCategory";

const Admin = () => {
    const [categoryVisible, setCategoryVisible] = useState(false)
    const [deleteCategoryVisible, setDeleteCategoryVisible] = useState(false)
    const [productVisible, setProductVisible] = useState(false)
    return (
        <Container className={'d-flex flex-column'}>
            <Button variant={"outline-dark"} className={'mt-3'} onClick={() => setCategoryVisible(true)}>Довавить категорию</Button>
            <Button variant={"outline-dark"} className={'mt-3'} onClick={() => setDeleteCategoryVisible(true)}>Удалить категорию</Button>
            <Button variant={"outline-dark"} className={'mt-3'} onClick={() => setProductVisible(true)}>Довавить Продукт</Button>
            <CreateCategory show={categoryVisible} onHide={() => setCategoryVisible(false)}/>
            <CreateProduct show={productVisible} onHide={() => setProductVisible(false)}/>
            <DeleteCategory show={deleteCategoryVisible} onHide={() => setDeleteCategoryVisible(false)}/>

        </Container>
    );
};

export default Admin;