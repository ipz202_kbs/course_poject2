import React, {useContext, useEffect, useState} from 'react';
import {Button, Card, Col, Container, Image, Row} from "react-bootstrap";
import {useParams} from 'react-router-dom'
import ProductService from "../services/ProductService";
import {Context} from "../index";
import CartService from "../services/CartService";
import ReviewList from "../components/ReviewList";
import ReviewForm from "../components/ReviewForm";
import {observer} from "mobx-react-lite";
import ChangeProduct from "../components/modals/ChangeProduct";


const ProductPage =() => {
    const [product, setProduct] = useState({});
    const {id} = useParams();
    const {user} = useContext(Context)

    const [changeVisible, setChangeVisible] = useState(false)

    const addToCart = async () => {
        if(!user.isAuth)
            return alert('Вы не авторизованы!')
        if (!user.getUser.user.isActivated)
            return alert('Ваш аккаунт не активирован!')
        await CartService.addProduct(user.getUser.user.id, id)
    }
    useEffect(() => {
        ProductService.fetchOneProduct(id).then(response => {
            setProduct(response.data)
        });
    },[changeVisible])

    return (
        <Container className="mt-3">
            <h2>{product.name}</h2>
            <Row>
                <Col md={4}>
                        <Image width={300} height={300} src={process.env.REACT_APP_API_URL+product.photo}/>
                </Col>
                <Col md={4} >
                    <div className="d-flex flex-column ps-3 justify-content-around" style={{width: 300, height: 300, fontSize: 15}}>
                        <p>Крепость: {product.fortress}%</p>
                        <p>Объем {product.volume}л</p>
                        <p>Лет выдержки {product.excerpt}</p>
                        <p>Страна производства {product.country_of_origin}</p>
                        <p>Оценка покупателей {product.total_rating}/5</p>
                    </div>
                </Col>
                <Col md={4} >
                    <Card className="d-flex flex-column align-items-center justify-content-around" style={{width: 300, height: 300, fontSize: 32}}>
                        <h4>Цена {product.price} грн.</h4>
                        <Button variant={"outline-dark"} onClick={addToCart}>Добавить в корзину</Button>
                        {(user.getRole === 'ADMIN' && user.isAuth) && <Button variant={"outline-dark"} className={'mt-3'} onClick={() => setChangeVisible(true)}>Изменить</Button>}
                        <ChangeProduct show={changeVisible} onHide={() => setChangeVisible(false)} id = {id} />
                    </Card>
                </Col>
            </Row>
            <h3>Описание</h3>
            <Row className="d-flex flex-column ms-auto">
                {product.description}
            </Row>
            <hr/>
            {user.isAuth && <ReviewForm/>}
            <Row className="d-flex mt-4">
                <h3 className="d-flex ms-1">Отзывы</h3>
            </Row>
            <Card className="d-flex flex-column p-3">
                <ReviewList items={product.reviews}/>
            </Card>
        </Container>
    );
};

export default observer(ProductPage);