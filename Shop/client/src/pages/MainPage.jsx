import React, {useContext, useEffect, useState} from 'react';
import {Card, Container, Form} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import CategoryBar from "../components/FilterBar";
import ProductList from "../components/ProductList";
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import CategoryService from "../services/CategoryService";
import ProductService from "../services/ProductService";
import {toJS} from "mobx";
import Button from "react-bootstrap/Button";


const MainPage = () => {
    const {product} = useContext(Context)
    const[minPrice, setMinPrice] = useState('')
    const[maxPrice, setMaxPrice] = useState('')
    const[minVolume, setMinVolume] = useState('')
    const[maxVolume, setMaxVolume] = useState('')
    const[minFortress,setMinFortress] = useState('')
    const[maxFortress,setMaxFortress] = useState('')

    useEffect(() => {
        CategoryService.fetchCategories().then(response => product.setCategories(response.data));
        ProductService.fetchProducts(false).then(response => product.setProduct(response.data));
    },[])

    const applyFilters = () =>{

        ProductService.fetchProducts(toJS(product.selectedCategory)._id, minPrice, maxPrice, minVolume, maxVolume,minFortress,maxFortress).then(response =>
        {product.setProduct(response.data)
        });
    }
    const resetFilters = () =>{
        ProductService.fetchProducts(false).then(response => {product.setProduct(response.data)});
        product.setSelectedCategory({})
        setMinPrice('');
        setMaxPrice('');
        setMinVolume('');
        setMaxVolume('');
        setMinFortress('');
        setMaxFortress('');
    }

    return (
        <Container>
            <Row className="mt-2">
                <Col md={3}>
                    <Card className="p-3 d-flex flex-column justify-content-center">
                        <CategoryBar/>
                        <div>Цена</div>
                        <div className="p-2 d-flex flex-lg-row justify-content-between">
                            <Form.Control
                                className="me-2"
                                placeholder="От"
                                min = '0'
                                type={'number'}
                                value={minPrice}
                                step = '50'
                                onChange={e => setMinPrice(Number(e.target.value))}
                            />
                            <Form.Control
                                className="ms-2"
                                placeholder="До"
                                min = '0'
                                type={'number'}
                                value={maxPrice}
                                step = '50'
                                onChange={e => setMaxPrice(Number(e.target.value))}
                            />
                        </div>
                        <hr/>
                        <div>Объем</div>
                        <div className="p-2 d-flex flex-lg-row justify-content-between">
                            <Form.Control
                                className="me-2"
                                placeholder="От"
                                min = '0'
                                type={'number'}
                                value={minVolume}
                                step = '0.1'
                                onChange={e => setMinVolume(Number(e.target.value))}
                            />
                            <Form.Control
                                className="ms-2"
                                placeholder="До"
                                min = '0'
                                type={'number'}
                                value={maxVolume}
                                step = '0.1'
                                onChange={e =>  setMaxVolume(Number(e.target.value))}
                            />
                        </div>
                        <hr/>
                        <div>Крепость напитка</div>
                        <div className="p-2 d-flex flex-lg-row justify-content-between">
                            <Form.Control
                                className="me-2"
                                placeholder="От"
                                min = '0'
                                type={'number'}
                                value={minFortress}
                                step = '1'
                                onChange={e => setMinFortress(Number(e.target.value))}
                            />
                            <Form.Control
                                className="ms-2"
                                placeholder="До"
                                min = '0'
                                type={'number'}
                                value={maxFortress}
                                step = '1'
                                onChange={e => setMaxFortress(Number(e.target.value))}
                            />
                        </div>
                        <hr/>
                        <div className="p-2 d-flex flex-lg-row justify-content-between">
                            <Button variant="outline-success"  onClick={applyFilters}>Применить</Button>
                            <Button variant="outline-danger" onClick={resetFilters}>Сбросить</Button>
                        </div>
                    </Card>
                </Col>
                <Col md={9}>
                    <ProductList/>
                </Col>
            </Row>
        </Container>
    );
};

export default observer(MainPage);