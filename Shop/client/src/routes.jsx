import Admin from "./pages/Admin";
import {ADMIN_ROUTE, CART_ROUTE, LOGIN_ROUTE, PRODUCT_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE} from "./utils/consts";
import Cart from "./pages/Cart";
import Auth from "./pages/Auth";
import MainPage from "./pages/MainPage";
import ProductPage from "./pages/ProductPage";
import Registration from "./pages/Registration";

export const authRoutes = [
    {
        path:ADMIN_ROUTE,
        Component:Admin
    },
    {
        path:CART_ROUTE,
        Component:Cart
    }
];
export const publicRoutes = [
    {
        path:REGISTRATION_ROUTE,
        Component:Registration
    },
    {
        path:LOGIN_ROUTE,
        Component:Auth
    },
    {
        path:SHOP_ROUTE,
        Component:MainPage
    },
    {
        path:PRODUCT_ROUTE + '/:id',
        Component:ProductPage
    }
];