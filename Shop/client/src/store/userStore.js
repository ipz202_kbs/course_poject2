import {makeAutoObservable} from "mobx";
import jwt_decode from "jwt-decode";
import AuthService from "../services/AuthService";
import axios from "axios";



export default class UserStore {
    isLoading = false;

    constructor() {
        this._user = {}
        this._role = null
        this._isAuth = false
        makeAutoObservable(this)
    }
    setRole(role) {
        this._role = role
    }

    setIsAuth(bool) {
        this._isAuth = bool
    }
    setUser(user) {
        this._user = user
    }
    setLoading(bool) {
        this.isLoading = bool;
    }

    get getRole() {
        return this._role
    }
    get isAuth() {
        return this._isAuth
    }
    get getUser() {
        return this._user
    }
    async registration(name, age, nickname, email, password){
        try {
            const response = await AuthService.registration(name, age, nickname, email, password);
            localStorage.setItem('token', response.data);
            const {data} = jwt_decode(response.data);
            this.setUser({user:data});
            this.setIsAuth(true);
            this.setRole(data.role);
            return data
        } catch (e) {
            return alert(e.response.data.message)
        }
    }

    async login(email, password) {
        try {
            const response = await AuthService.login(email, password);
            localStorage.setItem('token', response.data);
            const data = jwt_decode(response.data);
            this.setUser({user:data});
            this.setIsAuth(true);
            this.setRole(data.role);
        } catch (e) {
            return alert(e.response.data.message)

        }
    }
    async logout() {
        try {
            await AuthService.logout();
            localStorage.removeItem('token');
            this.setUser({})
            this.setIsAuth(false);
            this.setRole('');
        } catch (e) {
            return alert(e.response.data.message)
        }
    }
    async checkAuth() {
        this.setLoading(true);
        try {
            const response = await axios.get(`${process.env.REACT_APP_API_URL}api/user/refresh`, {withCredentials: true})
            localStorage.setItem('token', response.data);
            const data = jwt_decode(response.data);
            this.setUser({user:data});
            this.setIsAuth(true);
            this.setRole(data.role);
        } catch (e) {
            this.setUser({});
            this.setIsAuth(false);
            this.setRole(null);
            console.log(e.response.data.message);
        } finally {
            this.setLoading(false);
        }
    }
}