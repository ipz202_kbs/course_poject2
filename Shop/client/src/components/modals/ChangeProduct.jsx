import React, {useContext, useEffect, useState} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {Form} from "react-bootstrap";
import {Dropdown} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import ProductService from "../../services/ProductService";

const ChangeProduct = ({show, onHide, id}) => {

    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [fortress, setFortress] = useState('');
    const [volume, setVolume] = useState('');
    const [excerpt, setExcerpt] = useState(0);
    const [cOfOrigin, setCOfOrigin] = useState('');
    const [description, setDescription] = useState('');
    const [photo, setPhoto] = useState(null);

    const selectFile = e =>{
        setPhoto(e.target.files[0])
    }
    const addProd = () =>{
        const formData = new FormData();
        formData.append('id',id);
        formData.append('price',price);
        formData.append('name',name);
        formData.append('fortress',fortress);
        formData.append('volume',volume);
        formData.append('excerpt',excerpt);
        formData.append('country_of_origin',cOfOrigin);
        formData.append('description',description);
        formData.append('img',photo);
        ProductService.changeProduct(formData).then(onHide())
    }
    useEffect(() => {
        ProductService.fetchOneProduct(id).then(response => {
            setName(response.data.name)
            setPrice(response.data.price)
            setFortress(response.data.fortress)
            setVolume(response.data.volume)
            setExcerpt(response.data.excerpt)
            setCOfOrigin(response.data.country_of_origin)
            setDescription(response.data.description)
        });
    },[])
    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Изменить товар
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={"Название"}
                        className={'mt-1'}
                    />
                    <Form.Control
                        value={price}
                        onChange={e => setPrice(Number(e.target.value))}
                        placeholder={"Стоимость"}
                        type={'number'}
                        className={'mt-1'}
                        min = '0'
                        step = '0.1'
                    />
                    <Form.Control
                        value={fortress}
                        onChange={e => setFortress(Number(e.target.value))}
                        placeholder={"Крепость"}
                        type={'number'}
                        className={'mt-1'}
                        min = '0'
                        step = '0.1'
                    />
                    <Form.Control
                        value={volume}
                        onChange={e => setVolume(Number(e.target.value))}
                        placeholder={"Объём"}
                        type={'number'}
                        className={'mt-1'}
                        min = '0'
                        step = '0.1'
                    />
                    <Form.Control
                        value={excerpt}
                        onChange={e => setExcerpt(Number(e.target.value))}
                        placeholder={"Лет выдержки"}
                        type={'number'}
                        className={'mt-1'}
                        min = '0'

                    />
                    <Form.Control
                        value={cOfOrigin}
                        onChange={e => setCOfOrigin(e.target.value)}
                        placeholder={"Страна производства"}
                        className={'mt-1'}
                    />
                    <Form.Control
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        placeholder={"Описание"}
                        className={'mt-1'}
                    />
                    <Form.Control
                        onChange={selectFile}
                        placeholder={"Фото"}
                        type={'file'}
                        className={'mt-1'}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={onHide}>Закрыть</Button>
                <Button variant="outline-success" onClick={addProd}>Сохранить</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default observer(ChangeProduct);