import React, {useContext, useState} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {Dropdown, Form} from "react-bootstrap";
import CategoryService from "../../services/CategoryService";
import {Context} from "../../index";
import {observer} from "mobx-react-lite";


const DeleteCategory = ({show, onHide}) => {
    const {product} = useContext(Context);

    const [value, setValue] = useState('');
    const deleteCateg = () =>{
        console.log(product.selectedCategory.category_name)
        CategoryService.deleteCategory(product.selectedCategory.category_name).then(() => setValue(''))
        onHide();
    }
    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Какую категорию удалить? Внимание, все товары этой категории будут удалены.
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{product.selectedCategory.category_name || "Выберите категорию"}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {product.categories.map(category =>
                                <Dropdown.Item
                                    onClick={(e) => {product.setSelectedCategory(category);}}
                                    key={category._id}
                                >
                                    {category.category_name}
                                </Dropdown.Item>
                            )}
                        </Dropdown.Menu>
                    </Dropdown>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={onHide}>Закрыть</Button>
                <Button variant="outline-success"  onClick={deleteCateg}>Удалить</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default observer(DeleteCategory);