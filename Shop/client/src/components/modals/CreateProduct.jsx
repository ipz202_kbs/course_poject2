import React, {useContext, useState} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {Form} from "react-bootstrap";
import {Context} from "../../index";
import {Dropdown} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import ProductService from "../../services/ProductService";

const CreateProduct =  ({show, onHide}) => {
    const {product} = useContext(Context);

    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [fortress, setFortress] = useState('');
    const [volume, setVolume] = useState('');
    const [excerpt, setExcerpt] = useState('');
    const [cOfOrigin, setCOfOrigin] = useState('');
    const [description, setDescription] = useState('');
    const [photo, setPhoto] = useState(null);

    const selectFile = e =>{
        setPhoto(e.target.files[0])
    }

    const addProd = () =>{
        const formData = new FormData();
        formData.append('category',product.selectedCategory.category_name);
        formData.append('price',price);
        formData.append('name',name);
        formData.append('fortress',fortress);
        formData.append('volume',volume);
        formData.append('excerpt',excerpt);
        formData.append('country_of_origin',cOfOrigin);
        formData.append('description',description);
        formData.append('img',photo);
        ProductService.createProduct(formData).then(data => (console.log('')))
        onHide();
    }

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Добавить товар
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{product.selectedCategory.category_name || "Выберите категорию"}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {product.categories.map(category =>
                                <Dropdown.Item
                                    onClick={() => {product.setSelectedCategory(category);}}
                                    key={category._id}
                                >
                                    {category.category_name}
                                </Dropdown.Item>
                            )}
                        </Dropdown.Menu>
                    </Dropdown>

                    <Form.Control
                        value={name}
                        onChange={e => setName(e.target.value)}
                        placeholder={"Название"}
                        className={'mt-1'}
                    />
                    <Form.Control
                        value={price}
                        onChange={e => setPrice(Number(e.target.value))}
                        placeholder={"Стоимость"}
                        type={'number'}
                        className={'mt-1'}
                        min = '0'
                        step = '0.1'
                    />
                    <Form.Control
                        value={fortress}
                        onChange={e => setFortress(Number(e.target.value))}
                        placeholder={"Крепость"}
                        type={'number'}
                        className={'mt-1'}
                        min = '0'
                        step = '0.1'
                    />
                    <Form.Control
                        value={volume}
                        onChange={e => setVolume(Number(e.target.value))}
                        placeholder={"Объём"}
                        type={'number'}
                        className={'mt-1'}
                        min = '0'
                        step = '0.1'
                    />
                    <Form.Control
                        value={excerpt}
                        onChange={e => setExcerpt(Number(e.target.value))}
                        placeholder={"Лет выдержки"}
                        type={'number'}
                        className={'mt-1'}
                        min = '0'

                    />
                    <Form.Control
                        value={cOfOrigin}
                        onChange={e => setCOfOrigin(e.target.value)}
                        placeholder={"Страна производства"}
                        className={'mt-1'}
                    />
                    <Form.Control
                        as="textarea"
                        style={{ height: '100px' }}
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        placeholder={"Описание"}
                        className={'mt-1'}
                    />
                    <Form.Control
                        onChange={selectFile}
                        placeholder={"Фото"}
                        type={'file'}
                        className={'mt-1'}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={onHide}>Закрыть</Button>
                <Button variant="outline-success" onClick={addProd}>Добавить</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default observer(CreateProduct);