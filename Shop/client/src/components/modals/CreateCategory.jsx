import React, {useState} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {Form} from "react-bootstrap";
import CategoryService from "../../services/CategoryService";

const CreateCategory = ({show, onHide}) => {
    const [value, setValue] = useState('');
    const addCateg = () =>{
        CategoryService.createCategory({category: value}).then(() => setValue(''))
        onHide();
    }

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Создать категорию
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Control
                        value={value}
                        onChange={e => setValue(e.target.value)}
                        placeholder={"Введите название категории"}
                    />
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" onClick={onHide}>Закрыть</Button>
                <Button variant="outline-success"  onClick={addCateg}>Добавить</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateCategory;