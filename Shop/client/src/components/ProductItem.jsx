import React, {useContext} from 'react';
import {Card, Col} from "react-bootstrap";
import Image from "react-bootstrap/Image";
import {useNavigate} from "react-router-dom";
import {PRODUCT_ROUTE} from "../utils/consts";
import AddToCartButton from "./AddToCartButton";
import {Context} from "../index";

import DeleteProductButton from "./DeleteProductButton";
import {observer} from "mobx-react-lite";

const ProductItem = ({product}) => {
    const {user} = useContext(Context)

    const navigate = useNavigate();
    return (
        <Col md={2} className={"m-3"}>
            <Card style={{width: 150, cursor: 'pointer'}} border={"light"} onClick={() => navigate(PRODUCT_ROUTE + `/${product._id}`)}>
                <Image width={150} height={150} src={process.env.REACT_APP_API_URL + product.photo}/>
                <div className="text-black-50 mt-1 d-flex justify-content-between align-items-center">
                </div>
                <div>{product.name}</div>
            </Card>
            <div className="d-flex justify-content-between align-items-center ">
                {user.isAuth && <AddToCartButton productId = {product._id}/>}
                {(user.isAuth && user.getRole === 'ADMIN') && <DeleteProductButton productId = {product._id}/>}
            </div>
        </Col>
    );
};

export default observer(ProductItem);