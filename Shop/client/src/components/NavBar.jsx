import React, {useContext, useState} from 'react';
import {Context} from "../index";
import {ADMIN_ROUTE, CART_ROUTE, LOGIN_ROUTE, SHOP_ROUTE} from "../utils/consts";
import Container from "react-bootstrap/Container";
import {Button, Form} from "react-bootstrap";
import {NavLink, useNavigate} from "react-router-dom";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import {observer} from "mobx-react-lite";
import ProductService from "../services/ProductService";

const NavBar = () => {
    const {user} = useContext(Context)
    const {product} = useContext(Context)

    const navigate = useNavigate();
    const [name, setName] = useState('');

    const search = () =>{
        ProductService.fetchBySearch(name).then(response => product.setProduct(response.data));
    }

    const logOut = () =>{
        user.logout();
        user.setIsAuth(false);
    }
    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <NavLink to={SHOP_ROUTE} className={'text-color-white'}>Shop</NavLink>
                <Form className="d-flex">
                    <Form.Control
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                    <Button variant="outline-success" onClick={search}>Search</Button>
                </Form>
                {user.isAuth ?
                    <Nav className="ml-auto">
                        {user.getRole === 'ADMIN' && <Button variant={"outline-light"} className="me-2" onClick={() => navigate(ADMIN_ROUTE)}>Админ панель</Button>}
                        <Button variant={"outline-light"} className="me-2" onClick={() => {
                            if (!user.getUser.user.isActivated){
                                return alert('Ваш аккаунт не активирован!')
                            }
                            navigate(CART_ROUTE)
                        }
                        }>Корзина</Button>
                        <Button variant={"outline-light"}  onClick={() => logOut()}>Выйти</Button>
                    </Nav>
                    :
                    <Nav className="ml-auto" >
                        <Button variant={"outline-light"} onClick={() => navigate(LOGIN_ROUTE)}>Авторизация</Button>
                    </Nav>
                }
            </Container>
        </Navbar>
    );
}

export default observer(NavBar);