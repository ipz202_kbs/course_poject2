import React, {useContext, useState} from 'react';
import {observer} from "mobx-react-lite";
import {Card, FloatingLabel, Form} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {useParams} from "react-router-dom";
import {Context} from "../index";
import ReviewService from "../services/RevievService";

const ReviewForm = () => {
    const {id} = useParams();
    const {user} = useContext(Context)

    const [rating, setRating] = useState(5)
    const [comment, setComment] = useState('')

    const addReview =  () =>{
        ReviewService.createReview(user.getUser.user.id, id, rating, comment).then(response => {
            if(!response) {
                setComment('');
                return alert("Вы уже оставили отзыв!")
            }
            setComment('');
        })

    }
    return (
            <Form>
                <h5>Можете оставить отзыв</h5>
                <Form.Check
                    inline
                    label="1"
                    name="rating"
                    type={'radio'}
                    value={1}
                    onChange={e => setRating(Number(e.target.value))}
                />
                <Form.Check
                    inline
                    label="2"
                    name="rating"
                    type={'radio'}
                    value={2}
                    onChange={e => setRating(Number(e.target.value))}
                />
                <Form.Check
                    inline
                    label="3"
                    name="rating"
                    type={'radio'}
                    value={3}
                    onChange={e => setRating(Number(e.target.value))}
                />
                <Form.Check
                    inline
                    label="4"
                    name="rating"
                    type={'radio'}
                    value={4}
                    onChange={e => setRating(Number(e.target.value))}
                />
                <Form.Check
                    inline
                    label="5"
                    name="rating"
                    type={'radio'}
                    value={5}
                    onChange={e => setRating(Number(e.target.value))}
                />
                <Button variant="outline-success" className={"ms-3"} onClick={addReview}>Отправить</Button>
                <FloatingLabel controlId="floatingTextarea2" label="Можете оставить отзыв">
                    <Form.Control
                        as="textarea"
                        style={{ height: '100px' }}
                        className={'mt-2'}
                        value={comment}
                        onChange={e => setComment(e.target.value)}

                    />
                </FloatingLabel>
            </Form>
    );
};

export default observer(ReviewForm);