import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {Dropdown} from "react-bootstrap";
const FilterBar =  ()=> {
    const {product} = useContext(Context)

    return (
        <Dropdown>
            <Dropdown.Toggle>{product.selectedCategory.category_name || "Выберите категорию"}</Dropdown.Toggle>
            <Dropdown.Menu>
                {product.categories.map(category =>
                    <Dropdown.Item
                        onClick={(e) => {product.setSelectedCategory(category);}}
                        key={category._id}
                    >
                        {category.category_name}
                    </Dropdown.Item>
                )}
            </Dropdown.Menu>
        </Dropdown>
    );
}

export default observer(FilterBar);