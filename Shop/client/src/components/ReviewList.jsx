import React from 'react';
import ReviewItem from "./ReviewItem";

const ReviewList = ({items}) => {
    return (
        <div>
            {items && items.map(review =>
                <ReviewItem key={review._id} review={review}/>
            )}
        </div>
    );
};

export default ReviewList;