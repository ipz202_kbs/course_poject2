import React from 'react';
import {Card} from "react-bootstrap";

const ReviewItem = ({review}) => {
    return (
        <Card>
            <Card.Header as="h5">{review.user_nickname} - {review.grade} звёзд</Card.Header>
            <Card.Body>
                <Card.Text>
                    {review.review}
                </Card.Text>
            </Card.Body>
        </Card>
    );
};

export default ReviewItem;