import React, {useContext, useEffect, useState} from 'react';
import {Card, Col} from "react-bootstrap";
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import {Context} from "../index";
import CartService from "../services/CartService";
import {observer} from "mobx-react-lite";
import ProductService from "../services/ProductService";
import CartError from "./CartError";

const CartItem = ({item}) => {
    const [product, setProduct] = useState({});

    const {cart} = useContext(Context)
    const {user} = useContext(Context)
    useEffect( async() => {
        ProductService.fetchOneProduct(item.productId).then(response => {setProduct(response.data)})
    },[])

    const addToCart = async () => {
        if (!user.getUser.user.isActivated){
            return alert('Ваш аккаунт не активирован!')
        }
        const {data} = await CartService.addProduct(user.getUser.user.id, item.productId);
        cart.setTotal(data.subTotal)
        cart.setCartItems(data.items)
    }
    const deleteFromCart = async () => {
        if (!user.getUser.user.isActivated){
            return alert('Ваш аккаунт не активирован!')
        }
        const {data} =  await CartService.deleteProduct(user.getUser.user.id, item.productId)
        cart.setTotal(data.subTotal)
        cart.setCartItems(data.items)
    }
    return (
        <Card className="p-1 d-flex m-1">
            {product ?<Row className="d-flex justify-content-md-start pl-3 pr-3">
                <Col md={2}>
                    <Image width={100} height={100} src = {process.env.REACT_APP_API_URL + product.photo}/>
                </Col>
                <Col md={3} className="d-flex justify-content-center">
                    <h5 className="d-flex align-items-center ms-3">{product.name}</h5>
                </Col>
                <Col md={2} className="d-flex flex-column mt-3 ">
                    <div className="d-flex align-items-center">Колличество</div>
                    <div className="d-flex justify-content-between align-items-center ">
                        <Button size='sm' variant="outline-success" className={'px-2'} onClick={deleteFromCart}>-</Button>
                        <div className={'m-1'}>{item.quantity}</div>
                        <Button size='sm' variant="outline-success" className={'px-2'} onClick={addToCart}>+</Button>
                    </div>
                </Col>
                <Col md={4} className="d-flex flex-column justify-content-center">
                    <div className="d-flex align-items-center ms-3">{product.price} грн.</div>
                    <div className="d-flex align-items-center ms-3">Стоимость всех: {item.total.toFixed(2)}</div>
                </Col>
            </Row>:
                <CartError item={item}/>
            }
        </Card>

    );
};

export default observer(CartItem);