import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import UserStore from './store/userStore'
import ProductStore from './store/productStore'
import CartStore from './store/cartStore'

export const Context = createContext(null);


ReactDOM.render(
    <Context.Provider value  = {{
        user: new UserStore(),
        product: new ProductStore(),
        cart: new CartStore()
    }}>
        <App />
    </Context.Provider>,
  document.getElementById('root')
);
