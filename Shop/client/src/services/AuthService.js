import {$host} from "../http/index";

export default class AuthService {
    static async registration (name, age, nickname, email, password) {
        return await $host.post('api/user/registration', {name, age, nickname, email, password})
    }

    static async login (email, password) {
        return await $host.post('api/user/login', {email, password})
    }
    static async logout(){
        return await $host.post('api/user/logout')
    }

}