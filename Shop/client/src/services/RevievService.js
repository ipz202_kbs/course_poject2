import {$authHost, $host} from "../http/index";

export default class ReviewService {
    static async createReview(user_id, product_id, grade, review) {
        return await $authHost.post('api/reviews/', {user_id, product_id, grade, review})
    }
    // static async fetchReviews(product_id) {
    //     return await $host.get(`api/reviews/${product_id}`)
    // }
}