import {$authHost, $host} from "../http/index";

export default class ProductService {
    static async createProduct(product) {
        return await $authHost.post('api/product/', product)
    }
    static async changeProduct(product) {
        return await $authHost.put('api/product/', product)
    }
    static async deleteOneProduct (_id) {
        return await $authHost.delete(`api/product/${_id}`);
    }
    static async fetchProducts (categoryId, minPrice, maxPrice, minVolume, maxVolume,minFortress,maxFortress) {
        return await $host.get(`api/product`,{params:{categoryId, minPrice, maxPrice, minVolume, maxVolume,minFortress,maxFortress}})
    }
    static async fetchBySearch (name) {
        return await $host.get(`api/product/search?name=${name}`)
    }
    static async fetchOneProduct (_id) {
        return await $host.get(`api/product/${_id}`);
    }
}