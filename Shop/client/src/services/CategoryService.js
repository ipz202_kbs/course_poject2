import {$authHost, $host} from "../http/index";

export default class CategoryService {
    static async createCategory (category) {
        return await $authHost.post('api/category/create', category)
    }
    static async deleteCategory (category) {
        return await $authHost.delete(`api/category/${category}`)
    }
    static async fetchCategories () {
        return await $host.get('api/category/get')
    }
}