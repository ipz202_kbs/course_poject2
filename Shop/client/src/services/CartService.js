import {$authHost} from "../http/index";

export default class CartService {
    static async addProduct (user_id, product_id) {
        return await $authHost.post('api/cart/', {user_id, product_id})
    }
    static async deleteProduct (user_id, product_id) {
        return await $authHost.delete(`api/cart?user_id=${user_id}&product_id=${product_id}`)
    }
    static async fetchProducts (user_id) {
        return await $authHost.get(`api/cart/${user_id}`)
    }
}